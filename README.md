# Installing QGIS

This is a submission making sure that QGIS is installed and that I can show orthoimagery and the vector data. The image is located in the img directory. The picture below shows sidewalk vector data over orthoimagery of Miami University
![](/img/Miami_Uni_Sidewalks.jpg)

# Installing Anaconda

1. When installing Anaconda, I went to the Anaconda website and installed the command line installer for mac. Before this step, I made sure Xcode was up to date.
![](/img/Screen_Shot_2020-08-22_at_10.33.01_PM.png)

2. Since I installed the command line installer for anaconda, I did not have to open a seperate anaconda command prompt. I opened command prompt (I use iterm2), I set the envrionment variable and I run a source command to run conda.sh so I can run conda commands.
![](/img/Screen_Shot_2020-08-22_at_10.44.44_PM.png)
![](/img/Screen_Shot_2020-08-22_at_10.46.07_PM.png)

3. Next, I created a virtual environment for the CSE 620A class.
![](/img/Screen_Shot_2020-08-22_at_10.52.01_PM.png)

4. Now that the virtual environment is created, I need to activate it. This puts the name the the virtual environment on the left side on the line to indicate to the user that they are in a virtual environment.
![](/img/Screen_Shot_2020-08-22_at_10.55.28_PM.png)
![](/img/Screen_Shot_2020-08-22_at_10.55.44_PM.png)

5. A virtual environment allows me to install specific libraries and versions that are specific for my project. In this case, I needed to install the following libraries which are installed using conda's installer
 ![](/img/Screen_Shot_2020-08-22_at_10.59.14_PM.png)

6. To make sure that jupyter was working, I opened up iphython and then jupyter notebook.
 ![](/img/Screen_Shot_2020-08-22_at_11.09.28_PM.png)
  In jupyter notebook, I double checked to make sure it was using the correct python which is in the virtual environment.
![](/img/Screen_Shot_2020-08-22_at_11.11.22_PM.png)

7. Now that I know that jupyter is working, I open jupyter lab and open up a jupyter notebook
![](/img/Screen_Shot_2020-08-22_at_11.16.24_PM.png)
![](/img/Screen_Shot_2020-08-22_at_11.17.13_PM.png)

8. To make sure matplotlib plots are working, I create a plot. I can tell their were not any issues because the cell executed completely and sures the execution number on the left.
![](/img/Screen_Shot_2020-08-22_at_11.19.49_PM.png)

9. Next I make sure sklearn is installed properly.
![](/img/Screen_Shot_2020-08-22_at_11.23.17_PM.png)

10. Then skimage
![](/img/Screen_Shot_2020-08-22_at_11.25.07_PM.png)

11. Then rasterio
![](/img/Screen_Shot_2020-08-22_at_11.30.13_PM.png)

12. Then shapely
![](/img/Screen_Shot_2020-08-22_at_11.26.34_PM.png)

13. Then fiona
![](/img/Screen_Shot_2020-08-22_at_11.27.29_PM.png)

14. And lastly pandas
![](/img/Screen_Shot_2020-08-22_at_11.28.33_PM.png)
